<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Test_question_t1_opt extends Model
{
    use HasFactory;

    public function question()
    {
        return $this->belongsTo(Test_question_t1::class, 'id');
    }
}
