<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Test_participant extends Model
{
    use HasFactory;

    protected $fillable = [
        'token_code',
        'session_id',
        'participant_name',
        'participant_email',
        'participant_phone',
        'participant_from',
        'created_by',
        'updated_at',
        'deleted_at'
    ];
}
