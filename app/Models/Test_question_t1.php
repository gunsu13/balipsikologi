<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Test_question_t1 extends Model
{
    use HasFactory;

    public function options()
    {
        return $this->hasMany(Test_question_t1_opt::class, 'question');
    }
}
