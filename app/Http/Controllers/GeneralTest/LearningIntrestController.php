<?php

namespace App\Http\Controllers\GeneralTest;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Str, Validator, DB;
use TCPDF;

class LearningIntrestController extends Controller
{
    //
    public function index()
    {
        $session_id = Str::uuid();
        return view('learninginterest.index', compact('session_id'));
    }

    public function registerParticipant(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token_code' => 'required|exists:institution_partner_tests,token_code',
            'participant_name' => 'required',
            'participant_email' => 'required',
            'participant_phone' => 'numeric'
        ]);

        if (!$validator->passes()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()->all(),
                'data' => [],
            ], 400);
        }

        $tokenDetil = \App\Models\Institution_partner_test::select('institution_partner_tests.*','institution_partners.institution_name')
                                                            ->join('institution_partners','institution_partner_tests.institution_partner','=','institution_partners.id')
                                                            ->where('institution_partner_tests.token_code', $request->token_code)
                                                            ->first();
        // cek participant is already registered or not
        $participant = \App\Models\Test_participant::where('token_code', $request->token_code)->where('participant_email', $request->participant_email)->first();
        if($participant != false)
        {
            $storeParticipant = \App\Models\Test_participant::where('id', $participant->id)->update([
                'participant_name' => $request->participant_name,
                'participant_email' => $request->participant_email,
                'participant_phone' => $request->participant_phone,
                'participant_from' => $request->participant_from
            ]);

            return response()->json([
                'status' => true,
                'message' => 'Oke',
                'data' => $participant,
            ], 200);
        }

        // cek if token kode is already maximum of limit

        $participant = \App\Models\Test_participant::where('token_code',$request->token_code)->count();
        if($participant >= $tokenDetil->max_participant)
        {
            return response()->json([
                'status' => false,
                'message' => "Participant has reach limit amount, please contact your test provider",
                'data' => [],
            ], 401);
        }

        // store data participant
        $storeParticipant = \App\Models\Test_participant::create([
            'token_code' => $request->token_code,
            'session_id' => Str::uuid(),
            'participant_name' => $request->participant_name,
            'participant_email' => $request->participant_email,
            'participant_phone' => $request->participant_phone,
            'participant_from' => $request->participant_from,
            'created_by' => 1
        ]);

        return response()->json([
            'status' => true,
            'message' => 'Oke',
            'data' => $storeParticipant,
        ], 200);
    }

    public function test($session_id)
    {
        $participant = \App\Models\Test_participant::where('session_id', $session_id)->first();

        $tokenDetil = \App\Models\Institution_partner_test::select('institution_partner_tests.*','institution_partners.institution_name')
        ->join('institution_partners','institution_partner_tests.institution_partner','=','institution_partners.id')
        ->where('institution_partner_tests.token_code', $participant->token_code)
        ->first();

        if($participant == false)
        {
            abort(404, 'Resource not found');
        }

        return view('learninginterest.test', compact('participant','session_id','tokenDetil'));
    }

    public function testResult($session_id)
    {
        $participant = \App\Models\Test_participant::where('session_id', $session_id)->first();
        // abort jika participant tidak ditemukan
        if($participant == false)
        {
            abort(404, 'Resource not found');
        }

        $tokenDetil = \App\Models\Institution_partner_test::select('institution_partner_tests.*','institution_partners.institution_name')
        ->join('institution_partners','institution_partner_tests.institution_partner','=','institution_partners.id')
        ->where('institution_partner_tests.token_code', $participant->token_code)
        ->first();

        $varkResult = $this->generateVarkResult($session_id);
        $personalityResult = $this->getPersonalityResult($session_id);

        return view('learninginterest.final_result', compact('participant','session_id','tokenDetil','varkResult','personalityResult'));
    }


    public function testResultPDF($session_id)
    {

        $participant = \App\Models\Test_participant::where('session_id', $session_id)->first();
        // abort jika participant tidak ditemukan
        if($participant == false)
        {
            abort(404, 'Resource not found');
        }

        $tokenDetil = \App\Models\Institution_partner_test::select('institution_partner_tests.*','institution_partners.institution_name')
        ->join('institution_partners','institution_partner_tests.institution_partner','=','institution_partners.id')
        ->where('institution_partner_tests.token_code', $participant->token_code)
        ->first();

        $varkResult = $this->generateVarkResult($session_id);
        $personalityResult = $this->getPersonalityResult($session_id);

        $pdf = new TCPDF();

        $view = \View::make('learninginterest.report.final_result', compact('participant','session_id','tokenDetil','varkResult','personalityResult'));
        $html_content = $view->render();

        $pdf->SetTitle("E-Report Learning Interest Test By Bali Psikologi ". $participant->participant_name);
        $pdf->AddPage('P', 'A4');
        $pdf->writeHTML($html_content, true, false, true, false, '');

        return $pdf->Output('participant-'.$participant->session_id.'-'. date('Ymdhis') .'.pdf', 'D');
    }

    private function generateVarkResult($session_id)
    {
        $varkResult = DB::table('answers')
                        ->select('session_id', 'answer', DB::raw('COUNT(answer) AS rekap_jawab'))
                        ->selectRaw('(COUNT(answer) / 30) * 100 AS percentage')->join('test_question_t1s','answers.question_id','=','test_question_t1s.id')
                        ->where('test_question_t1s.test', 2)
                        ->where('session_id', $session_id)
                        ->groupBy('session_id', 'answer')
                        ->orderByDesc('percentage')
                        ->first();

        if($varkResult->answer == "V")
        {
            $summaries = "Visual";
            $text = "Tipe Pembelajar ini Lebih suka memahami materi dalam bentuk, mindmap, diagram, brosure, flowchart, menandai materi dengan stabilo berbagai warna tulisan dan gambar. Di dalam kelas, pembelajar visual biasanya memiliki catatan rinci yang menyeluruh, terorganisir, dan rapi serta cenderung duduk di depan. Namun mereka cenderung tidak menyukai pembelajaran yang hanya mengandalkan teks tanpa contoh serta alat peraga.";
            $image = "visual_learner.png";
        }

        if($varkResult->answer == "A")
        {
            $summaries = "Auditory";
            $text = "Pembelajar auditori adalah Pendengar yang baik, Ia lebih suka memahami ide dari orang lain, berdiskusi tentang berbagai topik dengan kelompok atau guru, menggunakan alat perekam, mendengarkan materi dan menggunakan cerita. Di dalam kelas, pembelajar Audiotory akan banyak belajar melalui presentasi selain itu untuk mengasah kemampuan mereka untuk dapat berkomunikasi dengan percaya diri.";
            $image = "auditory_learner.png";
        }

        if($varkResult->answer == "R")
        {
            $summaries = "Reading / Write";
            $text = "Pembelajar Read-Writing adalah pembelajar yang lebih memahami materi yang disampaikan dengan membaca materi, membuat ringkasan dari materi yang didapatkan. Untuk lebih memudahkan penyerapan pembelajaran mereka akan membuat list pembelajaran dan membuat catatan secara manual. Pembelajar Read-Writing memiliki daya ingat yang kuat terkait materi yang dipelajarinya.";
            $image = "read_and_write_learnet.png";
        }

        if($varkResult->answer == "K")
        {
            $summaries = "Kinestetik";
            $text = "Pembelajar kinestetik merupakan pembelajar praktikal yang lebih menyukai pembelajaran dengan langsung praktik di lapangan. Mereka akan mendapat pembelajaran dari melakukan percobaan-percobaan dari eksperimen yang mereka lakukan. Mereka lebih banyak melakukan akitivitas langsung dan berbicara dengan bahasa tubuh, dan isyarat tubuh. Mereka tidak terlalu suka teori dan ceramah yang detail, mereka lebih suka belajar dari langkah-langkah praktis.";
            $image = "KinestheticLearning_Style.png";
        }

        return [
            'varkResult' => $varkResult,
            'summary' => $summaries,
            'result_text' => $text,
            'image' => $image
        ];
    }

    private function getPersonalityResult($session_id) : array
    {
        $extrovertResult = DB::table('answers')
                            ->selectRaw('SUM(answer) AS total_value')
                            ->join('test_question_t1s','answers.question_id','=','test_question_t1s.id')
                            ->where('test_question_t1s.test', 3)
                            ->whereIn('test_question_t1s.question_queue', [1,2,5,7,8])
                            ->where('answers.session_id', $session_id)
                            ->first();

        $introvertResult =  DB::table('answers')
                            ->selectRaw('SUM(answer) AS total_value')
                            ->join('test_question_t1s','answers.question_id','=','test_question_t1s.id')
                            ->where('test_question_t1s.test', 3)
                            ->whereIn('test_question_t1s.question_queue', [3,4,6,9,10])
                            ->where('answers.session_id', $session_id)
                            ->first();

        if($extrovertResult->total_value > $introvertResult->total_value)
        {
            $kesimpulan = "EKSTROVERT";
        }else{
            $kesimpulan = "INTROVERT";
        }

        if($kesimpulan == "INTROVERT")
        {
            $text = "Kepribadian Introvert adalah pribadi yang lebih berfokus pada pikiran dan perasaan yang dimiliki. Kepribadian tipe  ini merupakan pribadi yang tenang, stabil, terstruktur dan sistematis. Dalam mengerjakan tugas-tugasnya, ia merupakan pribadi yang dapat diandalkan. Kepribadian ini cenderung kurang mampu mengungkapkan ide dan pemikiran yang dimilikinya sehingga rentan menjadi cemas kemudian pesimis dengan diri sendiri.";
        }

        if($kesimpulan == "EKSTROVERT")
        {
            $text = "Kepribadian Ekstrovert adalah pola perilaku terbuka dan ekspresif dengan orang-orang yang di sekitarnya. Tipe kepribadian ini merupakan pribadi yang ramah, luwes, dapat beradaptasi dengan mudah, keterbukaannya pada orang lain membuat pertemanan sosialnya semakin luas. Kepribadian Ekstrovert tidak segan mengungkapkan isi hati dan ide yang dimiliki, terkadang membuatnya mudah terbawa perasaan, sehingga rentan akan konflik.";
        }

        return [
            'extrovertResult' => $extrovertResult,
            'introvertResult' => $introvertResult,
            'summary' => $kesimpulan,
            'result_text' => $text
        ];

    }

    public function varkTest($session_id, $test_id)
    {
        $participant = \App\Models\Test_participant::where('session_id', $session_id)->first();

        if($participant == false)
        {
            abort(404, 'Resource not found');
        }

        $tokenDetil = \App\Models\Institution_partner_test::select('institution_partner_tests.*','institution_partners.institution_name')
        ->join('institution_partners','institution_partner_tests.institution_partner','=','institution_partners.id')
        ->where('institution_partner_tests.token_code', $participant->token_code)
        ->first();

        $testDetil = \App\Models\General_test::where('test_id', $test_id)->first();

        // cek apakah sudah ada yang terjawab bata test ini
        $alreadyAnswer = 0;
        $answers = DB::table('answers')
                        ->join('test_question_t1s','answers.question_id','=','test_question_t1s.id')
                        ->where('test_question_t1s.test', $testDetil->id)
                        ->where('answers.session_id', $session_id)
                        ->get();

        if(!$answers->isEmpty())
        {
            $alreadyAnswer = 1;
        }

        return view('learninginterest.vark', compact('tokenDetil','session_id','participant','testDetil','alreadyAnswer'));
    }

    public function varkReTest($session_id, $test_id)
    {
        $testDetil = \App\Models\General_test::where('test_id', $test_id)->first();
        \App\Models\Answer::join('test_question_t1s', 'answers.question_id', '=','test_question_t1s.id')
                                            ->where('test_question_t1s.test', $testDetil->id)
                                            ->where('answers.session_id', $session_id)
                                            ->delete();
        return redirect(url('/lit/vark').'/'.$session_id.'/'.$test_id);
    }

    public function varkTestQuestion($session_id, $test_id, $question_count)
    {
        $test = \App\Models\General_test::where('test_id', $test_id)->first();
        $test_question = \App\Models\Test_question_t1::with('options')->where('test', $test->id)->where('question_queue', $question_count)->first();

        if($test_question == false)
        {
            return response()->json([
                'status' => false,
                'message' => 'question not found',
                'data' => [],
            ], 200);
        }

        return response()->json([
            'status' => true,
            'message' => 'get data succes',
            'data' => $test_question,
        ], 200);
    }

    public function varkTestStoreAnswer(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'session_id' => 'required',
            'question_id' => 'required',
        ]);

        if (!$validator->passes()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()->all(),
                'data' => [],
            ], 400);
        }

        $answers = $request->input('answers');

        if(empty($answers))
        {
            return response()->json([
                'status' => false,
                'message' => "silahkan pilih jawaban",
                'data' => [],
            ], 400);
        }

        foreach ($answers as $index => $answer) {
            \App\Models\Answer::create([
                'session_id' => $request->session_id,
                'question_id' => $request->question_id,
                'answer' => $answer
            ]);
        }

        return response()->json([
            'status' => true,
            'message' => 'get data succes',
            'data' => [],
        ], 200);
    }

    public function varkTestResult($session_id, $test_id)
    {
        $testDetil = \App\Models\General_test::where('test_id', $test_id)->first();

        $varkResult = $this->generateVarkResult($session_id);

        $participant = \App\Models\Test_participant::where('session_id', $session_id)->first();
        // abort jika participant tidak ditemukan
        if($participant == false)
        {
            abort(404, 'Resource not found');
        }

        return view('learninginterest.vark_result', compact('varkResult','testDetil','session_id','participant'));
    }

    public function personalityTest($session_id, $test_id)
    {
        $participant = \App\Models\Test_participant::where('session_id', $session_id)->first();
        // abort jika participant tidak ditemukan
        if($participant == false)
        {
            abort(404, 'Resource not found');
        }

        $tokenDetil = \App\Models\Institution_partner_test::select('institution_partner_tests.*','institution_partners.institution_name')
        ->join('institution_partners','institution_partner_tests.institution_partner','=','institution_partners.id')
        ->where('institution_partner_tests.token_code', $participant->token_code)
        ->first();

        $testDetil = \App\Models\General_test::where('test_id', $test_id)->first();

        // cek apakah sudah ada yang terjawab bata test ini
        $alreadyAnswer = 0;
        $answers = DB::table('answers')
                        ->join('test_question_t1s','answers.question_id','=','test_question_t1s.id')
                        ->where('test_question_t1s.test', $testDetil->id)
                        ->where('answers.session_id', $session_id)
                        ->get();

        if(!$answers->isEmpty())
        {
            $alreadyAnswer = 1;
        }
        return view('learninginterest.personalityTest', compact('tokenDetil','session_id','participant','testDetil','alreadyAnswer'));
    }


    public function personalityReTest($session_id, $test_id)
    {
        $testDetil = \App\Models\General_test::where('test_id', $test_id)->first();

        \App\Models\Answer::join('test_question_t1s', 'answers.question_id', '=','test_question_t1s.id')
                                            ->where('test_question_t1s.test', $testDetil->id)
                                            ->where('answers.session_id', $session_id)
                                            ->delete();

        return redirect(url('/lit/kepribadian').'/'.$session_id.'/'.$test_id);
    }

    public function personalityTestQuestion($session_id, $test_id, $question_count)
    {
        $test = \App\Models\General_test::where('test_id', $test_id)->first();
        $test_question = \App\Models\Test_question_t1::with('options')->where('test', $test->id)->where('question_queue', $question_count)->first();

        if($test_question == false)
        {
            return response()->json([
                'status' => false,
                'message' => 'question not found',
                'data' => [],
            ], 200);
        }

        return response()->json([
            'status' => true,
            'message' => 'get data succes',
            'data' => $test_question,
        ], 200);
    }

    public function personalityTestStore(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'session_id' => 'required',
            'question_id' => 'required',
            'answer' => 'required',
        ]);

        if (!$validator->passes()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()->all(),
                'data' => [],
            ], 400);
        }

        \App\Models\Answer::create([
            'session_id' => $request->session_id,
            'question_id' => $request->question_id,
            'answer' => $request->answer
        ]);

        return response()->json([
            'status' => true,
            'message' => 'get data succes',
            'data' => [],
        ], 200);
    }

    public function personalityTestResult($session_id, $test_id)
    {
        $testDetil = \App\Models\General_test::where('test_id', $test_id)->first();
        $personalityResult = $this->getPersonalityResult($session_id);
        $participant = \App\Models\Test_participant::where('session_id', $session_id)->first();
        // abort jika participant tidak ditemukan
        if($participant == false)
        {
            abort(404, 'Resource not found');
        }
        return view('learninginterest.personalityTest_result', compact('personalityResult','testDetil','session_id','participant'));
    }

}
