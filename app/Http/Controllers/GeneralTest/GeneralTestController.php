<?php

namespace App\Http\Controllers\GeneralTest;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\General_test;
use App\Models\Test_question_t1;
use Str, Validator, DB;

class GeneralTestController extends Controller
{
    //
    public function index($test_id)
    {
        $test = General_test::where('test_id', $test_id)->first();
        $soals = Test_question_t1::with('options')->where('test', $test->id)->get();

        $session_id = Str::uuid();

        return view('bahasacinta.index', compact('test', 'soals','session_id'));
    }

    public function getQuestion($test_id, $count)
    {
        $test = General_test::where('test_id', $test_id)->first();
        $test_question = Test_question_t1::with('options')->where('test', $test->id)->where('question_queue', $count)->first();

        if($test_question == false)
        {
            return response()->json([
                'status' => false,
                'message' => 'question not found',
                'data' => [],
            ], 200);
        }

        return response()->json([
            'status' => true,
            'message' => 'get data succes',
            'data' => $test_question,
        ], 200);
    }

    public function storeQuestion(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'session_id' => 'required',
            'question_id' => 'required'
        ]);

        if (!$validator->passes()) {
            return response()->json([
                'status' => false,
                'message' => $validator->errors()->all(),
                'data' => [],
            ], 400);
        }

        \App\Models\Answer::create([
            'session_id' => $request->session_id,
            'question_id' => $request->question_id,
            'answer' => $request->answer
        ]);

        return response()->json([
            'status' => true,
            'message' => 'get data succes',
            'data' => [],
        ], 200);
    }

    public function result($test_id, $session_id)
    {
        $test = General_test::where('test_id', $test_id)->first();
        $answerPercentages = DB::table('answers')
                        ->select('session_id', 'answer', DB::raw('COUNT(answer) AS rekap_jawab'))
                        ->selectRaw('(COUNT(answer) / 30) * 100 AS percentage')
                        ->where('session_id', $session_id)
                        ->groupBy('session_id', 'answer')
                        ->orderByDesc('percentage')
                        ->get();

        return view('bahasacinta.result', compact('answerPercentages','test'));
    }

    public function calculate(Request $request, $test_id)
    {
        $answers = $request->input('answers', []);

       // Initialize counters for each answer
        $answerCount = [
            'A' => 0,
            'B' => 0,
            'C' => 0,
            'D' => 0,
            'E' => 0,
            // Add more letters if needed
        ];

        // Loop through the answers and count occurrences
        foreach ($answers as $answer) {
            // Make sure the answer is a valid letter
            if (in_array($answer, ['A', 'B', 'C', 'D', 'E'])) {
                $answerCount[$answer]++;
            }
        }
        // Total number of questions
        $totalQuestions = 30;

        // Calculate percentages
        $answerPercentages = [];
        foreach ($answerCount as $letter => $count) {
            $percentage = ($count / $totalQuestions) * 100;
            $answerPercentages[$letter] = $percentage;
        }

        arsort($answerPercentages);

        return view('bahasacinta.result', compact('answerPercentages'));

    }
}
