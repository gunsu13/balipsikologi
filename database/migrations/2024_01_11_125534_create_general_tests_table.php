<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGeneralTestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_tests', function (Blueprint $table) {
            $table->id();
            $table->string('test_id', 36);
            $table->string('test_title', 250);
            $table->string('test_sub_title', 250);
            $table->text('test_description', 250);
            $table->text('test_image_baner')->nullable();
            $table->string('test_status', 20)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('general_tests');
    }
}
