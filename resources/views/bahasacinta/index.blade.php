<!DOCTYPE html>
<html>
<head>
  <title>{{ $test->test_title }} || Bali Psikologi App by Suarnainfotech</title>
  <link rel="stylesheet" href="{{ asset('css/bahasacinta.css') }}">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js">
</head>
<body>
    <div class="container">
        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12">
                <div class="card" style="width: 100%;">
                    <img class="card-img-top" src="{{ asset('image/Logo-Bali-Psikologi.png') }}" alt="Card image cap" style="width: 20%; margin-top:20px; margin-left:20px;">
                    <div class="card-body">
                      @if ($test->test_status == "active")
                        <h5 class="card-title">{{ $test->test_title }}</h5> <small>{{ $test->test_sub_title }}</small>
                        <p class="card-text">{{ $test->test_description }}</p>
                        <a href="#" class="btn btn-primary" id="btnTestStart">Yuk, Mulai</a>
                      @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 10px;" id="frmQuiz">
            <form action="#" id="frmQuizSubmit">
                @csrf
                <div class="col-md-12">
                    <div class="card" style="width: 100%;">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12" id="questionContainer">
                                    <p id="questionText"></p>

                                    <input type="hidden" id="session_id" name="session_id" value="{{ $session_id }}">
                                    <input type="hidden" id="question_count" name="question_count" value="1">
                                    <input type="hidden" id="question_id" name="question_id" value="1">
                                    <div style="margin-left: 20px;" id="questionOptions">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary" id="btnQuestionSubmit">Selanjutnya</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        {{-- <form action="{{ url('/general_test/5bahasacinta/') }}/{{ $test->test_id }}" method="post" id="frmQuiz">
            @csrf
            <div class="row" style="margin-top: 10px;">
                <div class="col-md-12">
                    <div class="card" style="width: 100%;">
                        <div class="card-body">
                            @foreach ($soals as $soal)
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <p>{{ $soal->question_queue }}. {{ $soal->question_text }}</p>
                                            @foreach ($soal->options as $jawaban)
                                                <div style="margin-left: 20px;">
                                                    <input type="radio" id="OPT_{{ $jawaban->question }}" name="answers[{{ $soal->id }}]" value="{{ $jawaban->option }}" required>
                                                    <label for="{{ $jawaban->option }}">{{ $jawaban->option_text }}</label><br>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="card-footer">

                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form> --}}
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>
        $('#btnTestStart').show();
        $('#frmQuiz').hide();

        $('#btnTestStart').click(function(){
            $('#frmQuiz').show();
            $('#btnTestStart').hide();

            $('#btnQuestionSubmit').prop('disabled', false);

            getQuestion();
            $('#question_count').val(parseInt($('#question_count').val())+1);
        })

        function getQuestion()
        {
            $.getJSON(`{{ url('/general_test/5bahasacinta') }}/{{ $test->test_id }}/getQuestion/`+$('#question_count').val(), function(obj){

                if(obj.status == false)
                {
                    $('#questionText').text("selesai, mengarahkan ke hasil")

                    window.location.href = `{{ url('/general_test/5bahasacinta') }}/{{ $test->test_id }}/viewResult/`+$('#session_id').val();
                    return false;
                }

                $('#questionText').text(obj.data.question_queue+" "+obj.data.question_text)
                $('#question_id').val(obj.data.id)

                $('#questionOptions').empty();
                var record = '';
                let i = 1;

                if (obj.data.options.length > 0)
                {
                    $.each( obj.data.options, function( key, value ) {
                        record += '<input type="radio" id="OPT_'+obj.data.options[key].question+'" name="answer" value="'+obj.data.options[key].option+'" required style="margin-right:10px;"><label for="'+obj.data.options[key].option+'"> '+obj.data.options[key].option_text+'</label><br>';

                    });
                }
                console.log(record);
                $('#questionOptions').append(record);
            })
        }

        $('#frmQuizSubmit').on('submit', function(e){
            e.preventDefault();

           let postUrl = `{{ url('/general_test/5bahasacinta') }}/{{ $test->test_id }}/storeQuestion`;

            $.ajax({
                url: postUrl,
                method:"POST",
                data:new FormData(this),
                dataType:'JSON',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                   $('#btnQuestionSubmit').prop('disabled',true);
                }
            }).done(function(obj) {

                // console.log(obj.status)
                // if(obj.status == false)
                // {
                //     location.href(`{{ url('/general_test/5bahasacinta') }}/{{ $test->test_id }}/viewResult/`+$('#session_id').val())
                // }

                $('#btnQuestionSubmit').prop('disabled', false);
                getQuestion();
                $('#question_count').val(parseInt($('#question_count').val())+1);

            }).fail(function (jqXHR, textStatus, error) {
                console.log("Post error: " + error);
            });
        })
    </script>
</body>
</html>
