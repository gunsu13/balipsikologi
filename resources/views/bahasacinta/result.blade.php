<!DOCTYPE html>
<html>
<head>
  <title>Result || Bali Psikologi App by Suarnainfotech</title>
  <link rel="stylesheet" href="{{ asset('css/bahasacinta.css') }}">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js">
</head>
<body>
    <div class="container">
        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12">
                <div class="card" style="width: 100%;">
                    <img class="card-img-top" src="{{ asset('image/Logo-Bali-Psikologi.png') }}" alt="Card image cap" style="width: 20%; margin-top:20px; margin-left:20px;">
                    <div class="card-body">
                        <h3>Hasil Test {{ $test->test_title }}</h3>
                        <br>
                        <table class="table">
                            @foreach ($answerPercentages as $answerp)
                                <tr>
                                    @if ($answerp->answer == "A")
                                        <td style="width: 20%;"><img class="card-img-top" src="{{ asset('image/WORD.png') }}" style="width: 50%;"></td>
                                        <td>Word of Affirmation (Kata-kata)</td>
                                    @endif
                                    @if ($answerp->answer == "B")
                                        <td style="width: 20%;"><img class="card-img-top" src="{{ asset('image/TIME.png') }}" style="width: 50%;"></td>
                                        <td>Quality Time (Kebersamaan yang berkualitas)</td>
                                    @endif
                                    @if ($answerp->answer == "C")
                                        <td style="width: 20%;"><img class="card-img-top" src="{{ asset('image/GIFT.png') }}" style="width: 50%;"></td>
                                        <td>Gifts</td>
                                    @endif
                                    @if ($answerp->answer == "D")
                                        <td style="width: 20%;"><img class="card-img-top" src="{{ asset('image/SERVICE.png') }}" style="width: 50%;"></td>
                                        <td>Act Of Service</td>
                                    @endif
                                    @if ($answerp->answer == "E")
                                        <td style="width: 20%;"><img class="card-img-top" src="{{ asset('image/TOUCH.png') }}" style="width: 50%;"></td>
                                        <td>Physical Touch</td>
                                    @endif
                                    <td><div style="font-size:30px;">{{ round($answerp->percentage) }}%</div></td>
                                </tr>
                        @endforeach
                        </table>
                        <a href="{{ url('/general_test/5bahasacinta/') }}/{{ $test->test_id }}" class="btn btn-primary">Test Ulang</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>

    </script>
</body>
</html>
