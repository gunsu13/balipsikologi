<!DOCTYPE html>
<html>
<head>
  <title>Learning Interest Test || Bali Psikologi App by Suarnainfotech</title>
  <link rel="stylesheet" href="{{ asset('css/bahasacinta.css') }}">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js">
  <link rel="stylesheet" href="{{ asset('css/general.css')}}">
</head>
<body>
    <div class="container-fluid">
        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12">
                <div class="card" style="width: 100%;">
                    <img class="card-img-top" src="{{ asset('image/Logo-Bali-Psikologi.png') }}" alt="Card image cap" style="width: 20%; margin-top:20px; margin-left:20px;">
                    <div class="card-body">
                        <h2 class="card-title">Personality Test</h2> <small></small>
                        <p class="card-text"></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12">
                <div class="card" style="width: 100%;">
                   <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <p style="text-align: justify;">{{ $testDetil->test_description }} </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                @if ($alreadyAnswer == 0)
                                    <a href="#" class="btn btn-primary" id="btnTestStart">Yuk, Mulai</a>
                                @endif
                                @if ($alreadyAnswer == 1) <p>Hi, test ini sudah dijawab.</p>
                                    {{-- <a href="{{ url('/lit/vark') }}/{{ $session_id }}/{{ $testDetil->test_id }}/reTest" class="btn btn-primary" id="btnViewResult">Lakukan Test Ulang</a> --}}
                                    <a href="{{ url('/lit/kepribadian') }}/{{ $session_id }}/{{ $testDetil->test_id }}/result" class="btn btn-primary" id="btnViewResult">Lihat Hasil Tes Ini</a>
                                @endif

                                <a href="{{ url('lit/test_list') }}/{{ $session_id }}" class="btn btn-danger pull-right" id="btnBackToMainMenu">Kembali Ke Menu Awal</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 10px;" id="frmQuiz">
            <form action="#" id="frmQuizSubmit">
                @csrf
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12" id="questionContainer">
                                    <p id="questionText"></p>

                                    <input type="hidden" id="session_id" name="session_id" value="{{ $session_id }}">
                                    <input type="hidden" id="question_count" name="question_count" value="1">
                                    <input type="hidden" id="question_id" name="question_id" value="1">
                                    <div style="margin-left: 20px;" id="questionOptions">

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="col-12">
                                <button type="submit" class="btn btn-primary" id="btnQuestionSubmit">Selanjutnya</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>
        $('#btnTestStart').show();
        $('#btnBackToMainMenu').show();
        $('#frmQuiz').hide();

        $('#btnTestStart').click(function(){
            $('#frmQuiz').show();
            $('#btnTestStart').hide();
            $('#btnBackToMainMenu').hide();

            $('#btnQuestionSubmit').prop('disabled', false);

            getQuestion();
            $('#question_count').val(parseInt($('#question_count').val())+1);
        })

        function getQuestion()
        {
            $.getJSON(`{{ url('/lit/kepribadian') }}/{{ $participant->session_id }}/{{ $testDetil->test_id }}/getQuestion/`+$('#question_count').val(), function(obj){

                if(obj.status == false)
                {
                    $('#questionText').text("selesai, mengarahkan ke hasil")

                    window.location.href = ``;
                    return false;
                }

                $('#questionText').text(obj.data.question_queue+" "+obj.data.question_text)
                $('#question_id').val(obj.data.id)

                $('#questionOptions').empty();
                var record = '';
                let i = 1;
                record += "<table class='table table-striped table-bordered'>";

                record += '<tr><td><input type="radio" id="OPT_1" name="answer" value="1" required style="margin-right:10px;"></td><td><label for="1">Sangat Tidak Setuju</label><br></td>';
                record += '<tr><td><input type="radio" id="OPT_2" name="answer" value="2" required style="margin-right:10px;"></td><td><label for="2">Tidak Setuju</label><br></td>';
                record += '<tr><td><input type="radio" id="OPT_3" name="answer" value="3" required style="margin-right:10px;"></td><td><label for="2">Cenderung Tidak Setuju</label><br></td>';
                record += '<tr><td><input type="radio" id="OPT_4" name="answer" value="4" required style="margin-right:10px;"></td><td><label for="4">Netral</label><br></td>';
                record += '<tr><td><input type="radio" id="OPT_5" name="answer" value="5" required style="margin-right:10px;"></td><td><label for="5">Cenderung Setuju</label><br></td>';
                record += '<tr><td><input type="radio" id="OPT_6" name="answer" value="6" required style="margin-right:10px;"></td><td><label for="6">Setuju</label><br></td>';
                record += '<tr><td><input type="radio" id="OPT_7" name="answer" value="7" required style="margin-right:10px;"></td><td><label for="7">Sangat Setuju</label><br></td>';

                record += "</table>";
                console.log(record);
                $('#questionOptions').append(record);
            })
        }

        $('#frmQuizSubmit').on('submit', function(e){
            e.preventDefault();

           let postUrl = `{{ url('/lit/kepribadian') }}/{{ $participant->session_id }}/{{ $testDetil->test_id }}/storeAnswer`;

            $.ajax({
                url: postUrl,
                method:"POST",
                data:new FormData(this),
                dataType:'JSON',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                   $('#btnQuestionSubmit').prop('disabled',true);
                }
            }).done(function(obj) {

                $('#btnQuestionSubmit').prop('disabled', false);
                getQuestion();
                $('#question_count').val(parseInt($('#question_count').val())+1);

            }).fail(function (jqXHR, textStatus, error) {
                console.log("Post error: " + error);
            });
        })
    </script>
</body>
</html>
