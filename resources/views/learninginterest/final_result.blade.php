<!DOCTYPE html>
<html>
<head>
  <title>Learning Interest Test || Bali Psikologi App by Suarnainfotech</title>
  <link rel="stylesheet" href="{{ asset('css/bahasacinta.css') }}">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js">
  <link rel="stylesheet" href="{{ asset('css/general.css')}}">
</head>
<body>
    <div class="container-fluid">
        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12">
                <div class="card" style="width: 100%;">
                    <img class="card-img-top" src="{{ asset('image/Logo-Bali-Psikologi.png') }}" alt="Card image cap" style="width: 20%; margin-top:20px; margin-left:20px;">
                    <div class="card-body">
                        <h2 class="card-title">Learning Interest and Personality Test</h2> <small></small>
                        <p class="card-text"></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12">
                <div class="card" style="width: 100%;">
                   <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Hi, {{ $participant->participant_name }} Kamu termasuk tipe</h3>
                                <h3>{{ $varkResult["summary"] }} - {{ $personalityResult["summary"] }}</h3>
                                <img class="card-img-top" src="{{ asset('image') }}/{{ $varkResult["image"] }}" style="width: 60%; margin: 0 auto; display: block;">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p><strong>A. Gaya Belajar</strong></p>
                                <p>Gaya belajar kamu adalah <b>{{ $varkResult["summary"] }}</b></p>
                                <p style="text-align:justify;">{{ $varkResult["result_text"] }}</p>
                            </div>
                            <div class="col-md-12">
                                <p><strong>B. Kepribadian</strong></p>
                                <p>Kepribadian kamu adalah<b> {{ $personalityResult["summary"] }}</b></p>
                                <p style="text-align:justify;">{{ $personalityResult["result_text"] }}</p>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <a href="{{ url('lit') }}/hasil/{{ $session_id }}/generatePDF" class="btn btn-danger pull-right" id="btnCetakPDF">Cetak PDF</a>
                                <a href="{{ url('lit/test_list') }}/{{ $session_id }}" class="btn btn-danger pull-right" id="btnBackToMainMenu">Kembali Ke Menu Awal</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12">
                <div class="card" style="width: 100%;">
                   <div class="card-body">
                        <div class="row"><div class="col-md-12">

                            <p>Kamu dapat Konsultasikan Hasil Test ini melalui kontak ini ya </p>
                        </div>
                            <div class="col-md-4">
                            </div>
                            <div class="col-md-4">
                                <a href="https://api.whatsapp.com/send/?phone=6289688882023&text&type=phone_number&app_absent=0" target="_blank">
                                    <img class="" src="{{ asset('image/WhatsApp_icon.png') }}" style="width: 40%; margin: 0 auto; display: block;">
                                </a>
                            </div>
                            <div class="col-md-4">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>

    </script>
</body>
</html>
