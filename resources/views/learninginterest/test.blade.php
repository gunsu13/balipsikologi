<!DOCTYPE html>
<html>
<head>
  <title>Learning Interest Test || Bali Psikologi App by Suarnainfotech</title>
  <link rel="stylesheet" href="{{ asset('css/bahasacinta.css') }}">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js">
  <link rel="stylesheet" href="{{ asset('css/general.css')}}">
</head>
<body>
    <div class="container-fluid">
        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12">
                <div class="card" style="width: 100%;">
                    <img class="card-img-top" src="{{ asset('image/Logo-Bali-Psikologi.png') }}" alt="Card image cap" style="width: 20%; margin-top:20px; margin-left:20px;">
                    <div class="card-body">
                        <h2 class="card-title">Learning Interest Test</h2> <small></small>
                        <p class="card-text"></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12">
                <div class="card" style="width: 100%;">
                   <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <p>Hi, <i>{{ $participant->participant_name }}</i> test ini bekerja sama dengan {{ $tokenDetil->institution_name }} berikut adalah 2 test untuk kamu ikuti, klik mulai jika sudah siap ya.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                               <h1>Test 1 : <a href="{{ url('/lit/vark/'. $session_id) }}/4f7dd75a-e041-11ee-a2a1-9c5c8e88f9b0" class="" id="btnTestStart">Test Minat Belajar</a></h1>
                            </div>
                            <div class="col-12">
                                <h1>Test 2 : <a href="{{ url('/lit/kepribadian/'. $session_id) }}/757cd386-e041-11ee-a2a1-9c5c8e88f9b0" class="" id="btnTestStart">Test Kepribadian</a></h1>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <h2>Lihat Hasil</h2>
                                <a href="{{ url('/lit/hasil') }}/{{ $session_id }}" class="btn btn-primary" id="btnTestStart">Hasil</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>
    </script>
</body>
</html>
