<!DOCTYPE html>
<html>
<head>
    <title>E-Report Learning Interest Test By Bali Psikologi {{ $participant->participant_name }}</title>
    <style>
        html, body {
            height: 100%;
            margin: 0;
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .container {
            width: 100%;
            max-width: 600px; /* Adjust max-width as needed */
            text-align: center;
        }
        .logo {
            margin-top: 20px;
        }
    </style>
</head>
<body>
    <div class="container">
        <table style="width: 100%" border="0">
            <tr>
                <td style="width:20%;">
                    <!-- Centered and middle aligned image -->
                    <img src="{{ public_path('image/') }}/Logo-Bali-Psikologi.png" style="width:100px;" class="logo">
                </td>
                <td style="width:80%;">
                    <h4>Praktek Psikologi Klinis</h4>
                    <h1>Bali Psikologi</h1>
                    <p>Jl. Gunung Talang No.70, Padangsambian<br>0896-8888-2023 - balipsikologi@gmail.com - IG : @balipsikologi </p>
                </td>
            </tr>
        </table>
        <br>
    </div>
    <h3><i>Participant Detil</i></h3>
    <table style="width: 100%" border="0">
        <tr>
           <td style="width: 10%">Nama</td>
           <td style="width: 40%">{{ $participant->participant_name }}</td>
           <td style="width: 10%">Sekolah</td>
           <td style="width: 40%">{{ $participant->participant_from }}</td>
        </tr>
        <tr>
           <td>Email</td>
           <td>{{ $participant->participant_email }}</td>
           <td>No tlp</td>
           <td>{{ $participant->participant_phone }}</td>
        </tr>
    </table>
    <div class="row">
        <div class="col-md-12">
            <h3>Hi, {{ $participant->participant_name }} Kamu termasuk tipe</h3>
            <h3>{{ $varkResult["summary"] }} - {{ $personalityResult["summary"] }}</h3>
            <img src="{{ public_path('image') }}/{{ $varkResult["image"] }}" style="width:300px" class="logo">
        </div>
        <div class="col-md-12">
            <h4>A. Gaya Belajar</h4>
            <p>Gaya belajar kamu adalah <b>{{ $varkResult["summary"] }}</b></p>
            <p style="text-align: justify;">{{ $varkResult["result_text"] }}</p>
            <h4>B. Kepribadian</h4>
            <p>Kepribadian kamu adalah<b> {{ $personalityResult["summary"] }}</b></p>
            <p style="text-align: justify;">{{ $personalityResult["result_text"] }}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p>Test ini dikembangkan oleh Bali Psikologi dengan bekerja sama dengan penyelenggara test yaitu <i>{{ $tokenDetil->institution_name }}</i>, hasil ini dapat dilihat online pada <a href="{{ url('lit') }}/hasil/{{ $session_id }}" target="_blank">Link Berikut</a></p>
            <p>Kamu dapat Konsultasikan Hasil Test ini melalui kontak Admin <a href="https://api.whatsapp.com/send/?phone=6289688882023&text&type=phone_number&app_absent=0" target="_blank">Klik Disini</a> </p>
        </div>
    </div>
</body>
</html>
