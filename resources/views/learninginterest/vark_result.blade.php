<!DOCTYPE html>
<html>
<head>
  <title>Learning Interest Test || Bali Psikologi App by Suarnainfotech</title>
  <link rel="stylesheet" href="{{ asset('css/bahasacinta.css') }}">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js">
  <link rel="stylesheet" href="{{ asset('css/general.css')}}">
</head>
<body>
    <div class="container-fluid">
        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12">
                <div class="card" style="width: 100%;">
                    <img class="card-img-top" src="{{ asset('image/Logo-Bali-Psikologi.png') }}" alt="Card image cap" style="width: 20%; margin-top:20px; margin-left:20px;">
                    <div class="card-body">
                        <h5 class="card-title">Learning Interest Test</h5> <small></small>
                        <p class="card-text"></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12">
                <div class="card" style="width: 100%;">
                   <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>Hasil Test Gaya Belajar, {{ $participant->participant_name }}</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <p>Gaya belajar kamu adalah <b>{{ $varkResult["summary"] }}</b></p>
                                <img class="" src="{{ asset('image') }}/{{ $varkResult["image"] }}" style="width: 60%; margin: 0 auto; display: block;">
                                <p style="text-align: justify;">{{ $varkResult["result_text"] }}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <a href="{{ url('lit/test_list') }}/{{ $session_id }}" class="btn btn-danger pull-right" id="btnBackToMainMenu">Kembali Ke Menu Awal</a>
                                <a href="{{ url('lit/kepribadian') }}/{{ $session_id }}/757cd386-e041-11ee-a2a1-9c5c8e88f9b0" class="btn btn-info pull-right" id="btnBackToMainMenu">Ikuti test selanjutnya </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>

    </script>
</body>
</html>
