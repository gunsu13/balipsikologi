<!DOCTYPE html>
<html>
<head>
  <title>Learning Interest Test || Bali Psikologi App by Suarnainfotech</title>
  <link rel="stylesheet" href="{{ asset('css/bahasacinta.css') }}">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js">
  <link rel="stylesheet" href="{{ asset('css/general.css')}}">
</head>
<body>
    <div class="container-fluid">
        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12">
                <div class="card" style="width: 100%;">
                    <img class="card-img-top" src="{{ asset('image/Logo-Bali-Psikologi.png') }}" alt="Card image cap" style="width: 20%; margin-top:20px; margin-left:20px;">
                    <div class="card-body">
                        <h2 class="card-title">Learning Interest and Personality Test</h2> <small>Test gaya belajar dan kepribadian</small>
                        <p class="card-text"></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row" style="margin-top: 10px;">
            <div class="col-md-12">
                <div class="card" style="width: 100%;">
                   <div class="card-body">
                        <form id="formRegisterParticipant">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <p>Sebelum melakukan test, di isi terlebih dahulu form berikut :</p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="token_code">Kode Token Test**</label>
                                        <input type="text" name="token_code" id="token_code" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="participant_name">Nama Peserta*</label>
                                        <input type="text" name="participant_name" id="participant_name" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="participant_email">Email*</label>
                                        <input type="email" name="participant_email" id="participant_email" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="participant_phone">No Hp</label>
                                        <input type="text" name="participant_phone" id="participant_phone" class="form-control" required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="participant_from">Sekolah</label>
                                        <input type="text" name="participant_from" id="participant_from" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary" id="btnTestStart">Yuk, Mulai</button>
                                </div>
                            </div>
                        </form>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                *) Wajib di isi <br>
                                **) Kode Token merupakan kode yang diberikan oleh institusi mitra test
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script>
        $('#formRegisterParticipant').on('submit', function(e){
            e.preventDefault();

           let postUrl = `{{ url('/lit') }}`;

            $.ajax({
                url: postUrl,
                method:"POST",
                data:new FormData(this),
                dataType:'JSON',
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function() {
                   $('#btnTestStart').prop('disabled',true);
                }
            }).done(function(obj) {
                window.location.href =  `{{ url("/lit/test_list") }}/`+obj.data.session_id;
            }).fail(function (jqXHR, textStatus, error) {
                alert(jqXHR.responseJSON.message);
            });
        })
    </script>
</body>
</html>
