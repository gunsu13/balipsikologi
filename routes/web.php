<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GeneralTest\GeneralTestController;
use App\Http\Controllers\GeneralTest\LearningIntrestController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/general_test/5bahasacinta/{test_id}', [GeneralTestController::class, 'index']);
Route::post('/general_test/5bahasacinta/{test_id}', [GeneralTestController::class, 'calculate']);
Route::get('/general_test/5bahasacinta/{test_id}/getQuestion/{count}', [GeneralTestController::class, 'getQuestion']);
Route::post('/general_test/5bahasacinta/{test_id}/storeQuestion', [GeneralTestController::class, 'storeQuestion']);
Route::get('/general_test/5bahasacinta/{test_id}/viewResult/{session_id}', [GeneralTestController::class, 'result']);

Route::get('/lit', [LearningIntrestController::class, 'index']);
Route::post('/lit', [LearningIntrestController::class, 'registerParticipant']);
Route::get('/lit/test_list/{session_id}', [LearningIntrestController::class, 'test']);
Route::get('/lit/vark/{session_id}/{test_id}', [LearningIntrestController::class, 'varkTest']);
Route::get('/lit/vark/{session_id}/{test_id}/getQuestion/{question_count}', [LearningIntrestController::class, 'varkTestQuestion']);
Route::post('/lit/vark/{session_id}/{test_id}/storeAnswer', [LearningIntrestController::class, 'varkTestStoreAnswer']);
Route::get('/lit/vark/{session_id}/{test_id}/result', [LearningIntrestController::class, 'varkTestResult']);
Route::get('/lit/vark/{session_id}/{test_id}/reTest', [LearningIntrestController::class, 'varkReTest']);
Route::get('/lit/kepribadian/{session_id}/{test_id}', [LearningIntrestController::class, 'personalityTest']);
Route::get('/lit/kepribadian/{session_id}/{test_id}/reTest', [LearningIntrestController::class, 'personalityReTest']);
Route::get('/lit/kepribadian/{session_id}/{test_id}/getQuestion/{question_count}', [LearningIntrestController::class, 'personalityTestQuestion']);
Route::post('/lit/kepribadian/{session_id}/{test_id}/storeAnswer', [LearningIntrestController::class, 'personalityTestStore']);
Route::get('/lit/kepribadian/{session_id}/{test_id}/result', [LearningIntrestController::class, 'personalityTestResult']);
Route::get('/lit/hasil/{session_id}', [LearningIntrestController::class, 'testResult']);
Route::get('/lit/hasil/{session_id}/generatePDF', [LearningIntrestController::class, 'testResultPDF']);
